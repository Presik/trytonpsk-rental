# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'
    factor = fields.Numeric('Factor')

    @fields.depends('type', 'quantity', 'unit_price', 'invoice',
        '_parent_invoice.currency', 'currency', 'factor')
    def on_change_with_amount(self):
        amount_ = super(InvoiceLine, self).on_change_with_amount()
        if self.factor:
            amount_ = amount_ * self.factor
        return amount_
